var validator = require("email-validator");
var mysqlcreateConnection = require('../../exports/mysql.js');

var create = function(request, response) {
	var urlObject = request.body;
	var responseJSON;
	var emailValidate = validator.validate(urlObject.email);
	if(urlObject.email && urlObject.first_name && urlObject.gender) {
		if(!emailValidate) {
			sendResponse(response, 400, 'Please enter valid email');
			return;
		}
		var result = mysqlcreateConnection.insertIntoUserTable(urlObject, function(error, result) {
			if(error) {
				sendResponse(response, 400, error);
			} else {
				sendResponse(response, 200, {userId: result.insertId});
			}
		});
	} else {
		sendResponse(response, 400, 'Please fill all mandatory details');
	}
}

var getAllRecords = function(request, response) {
	mysqlcreateConnection.getAllUsers(function(error, result) {
		if(error) {
			//response.json({message: error});
			sendResponse(response, 400, error);
		} else {
			sendResponse(response, 200, result);
		}
	});
}

var updateTable = function(request, response) {
	var urlObject = request.body;
	mysqlcreateConnection.updateUserTable(urlObject, function(error, result) {
		if(error) {
			sendResponse(response, 400, error);
		} else {
			sendResponse(response, 200, result);
		}
	});
}

var login = function(request, response) {
	var urlObject = request.query;
	mysqlcreateConnection.authenticateUser(urlObject, function(error, result) {
		if(error) {
			sendResponse(response, 400, error);
		} else {
			sendResponse(response, 200, result);
		}
	})
}

var sendResponse = function (response, statusCode, message) {
	response.status(statusCode);
	var statusMessage =  statusCode === 200 ? 'Success' : 'Failure';
	var responseJSON = {'status': statusMessage, 'message': message};
	response.json(responseJSON);
}


module.exports.create = create;
module.exports.getAllRecords = getAllRecords;
module.exports.updateTable = updateTable;
module.exports.login = login;