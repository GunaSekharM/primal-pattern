var express = require('express');
var bodyParser = require('body-parser');
var jsonwebtoken = require('jsonwebtoken');

var multiparty = require('connect-multiparty');


var mysqlcreateConnection = require('../exports/mysql.js');
var userController = require('./controller/userController.js');

var app = express();
var userRouter = express.Router();
var multipartyMiddleware = multiparty();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(multipartyMiddleware);
// app.use(fileUpload());

let port = process.env.PORT || 3000;

mysqlcreateConnection.createConnection();


var sendResponse = function (response, statusCode, message) {
	response.status(statusCode);
	var statusMessage =  statusCode === 200 ? 'Success' : 'Failure';
	var responseJSON = {'status': statusMessage, 'message': message};
	response.json(responseJSON);
}

userRouter.route('/signup')
	.post(function(request, response) {
		userController.create(request, response);
	});

userRouter.route('/users')
	.get(function(request, response) {
		userController.getAllRecords(request, response);
	});

userRouter.route('/login')
	.get(function(request, response) {
		userController.login(request, response);
	});

userRouter.route('/uploadFile')
	.post(function(request, response) {
		console.log('request', request);
		mysqlcreateConnection.uploadfileinAws(request, function(error, result) {
			if(error) {
				sendResponse(response, 400, error);
			} else {
				sendResponse(response, 200, result);
			}
		})
	});

userRouter.route('/updateUserInfo')
	.post(function(request, response) {
		userController.updateTable(request, response);
	});

userRouter.route('/dailyReport')
	.get(function(request, response) {
		var urlObject = request.query;
		mysqlcreateConnection.getDailyReport(urlObject, function(error, result) {
			if(error) {
				sendResponse(response, 400, error);
			} else {
				sendResponse(response, 200, result);
			}
		});
	});

userRouter.route('/getSevenDays')
	.get(function(request, response) {
		var urlObject = request.query;
		mysqlcreateConnection.getSevenReport(urlObject, function(error, result) {
			if(error) {
				sendResponse(response, 400, error);
			} else {
				sendResponse(response, 200, result);
			}
		});
	});

app.use('/api', userRouter);

app.listen(port, function() {
	console.log('server running on port ' + port);
});