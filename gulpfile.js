let gulp = require('gulp');
let nodemon = require('gulp-nodemon');

gulp.task('default', function() {
	nodemon({
		script: './app/app.js',
		ext: 'js',
		env: {
			PORT: 8080
		},
		ignore: ['./node_module/**']
	})

	.on('restart', function() {
		console.log('Server restarted');
	})
})